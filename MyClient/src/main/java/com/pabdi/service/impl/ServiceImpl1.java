package com.pabdi.service.impl;

import com.pabdi.service.Service;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServiceImpl1 implements Service {

    @Override
    public void get(String url) {
        URL myUrlToGet = null;
        try {
            //send the request
            myUrlToGet = new URL(url);
            HttpURLConnection myUrlConToGet = (HttpURLConnection) myUrlToGet.openConnection();
            myUrlConToGet.setRequestProperty("Content-Type", "application/json; utf-8");
            myUrlConToGet.setRequestProperty("Accept", "application/json");
            myUrlConToGet.setDoOutput(true);
            myUrlConToGet.setRequestMethod("GET");

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(myUrlConToGet.getInputStream(), "utf-8"))) {
                //get the response
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                //parse the response
                String result = response.toString();
                JSONParser parser = new JSONParser();
                JSONObject post = (JSONObject) parser.parse(result);
                Long id = (Long) post.get("id");
                Long userId = (Long) post.get("userId");
                String title = (String) post.get("title");
                String body = (String) post.get("body");
                System.out.println("Article " + id + ": User " + userId + " Title \"" + title + "\" Message \"" + body + "\"");
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void post(String url) {
        try {
            //send the request
            URL myUrlToPost = new URL(url);
            HttpURLConnection myUrlConToPost = (HttpURLConnection) myUrlToPost.openConnection();
            String jsonInputString = "{\"title\": \"some title\", \"body\": \"some message\", \"userId\": 1}";

            myUrlConToPost.setRequestMethod("POST");
            myUrlConToPost.setRequestProperty("Content-Type", "application/json");
            myUrlConToPost.setRequestProperty("Accept", "application/json");
            myUrlConToPost.setDoOutput(true);
            myUrlConToPost.setDoInput(true);

            try (OutputStream os = myUrlConToPost.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input);
                os.flush();
            }

            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(myUrlConToPost.getInputStream(), "utf-8"))) {
                //get the response
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine);
                }

                //parse the response
                String result = response.toString();
                JSONParser parser = new JSONParser();
                JSONObject post = (JSONObject) parser.parse(result);
                Long id = (Long) post.get("id");
                Long userId = (Long) post.get("userId");
                String title = (String) post.get("title");
                String body = (String) post.get("body");
                System.out.println("Article " + id + " has been created: User " + userId + " Title \"" + title + "\" Message \"" + body + "\"");
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
