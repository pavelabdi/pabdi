package com.pabdi;

public class NoWaterException extends Exception {

    public NoWaterException(){
        super("Not enough water");
    }
}
