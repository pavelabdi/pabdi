package com.pabdi;

import lombok.EqualsAndHashCode;

import java.util.concurrent.Semaphore;

@EqualsAndHashCode
public class Waiter extends Semaphore {

    public Waiter(int permits) {
        super(permits);
    }
}
