package com.pabdi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Main {

    private static final int count = 5;

    private static Waiter[] waiters = {new Waiter(4), new Waiter(4)};

    public static void main(String[] args) {

        File file = new File("out.txt");
        if (file.exists()) {
            file.delete();
        }

        try {
            System.setOut(new PrintStream(new FileOutputStream("out.txt", true)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Fork[] forks = new Fork[count];

        for (int i = 0; i < count; i++) {
            forks[i] = new Fork(1);
        }

        for (int i = 0; i < count; i++) {
            new Thread(new Philosopher(i, forks[i], forks[(i + 1) % count], waiters)).start();
        }
    }
}
