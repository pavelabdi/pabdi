package com.pabdi;

import java.util.Scanner;

/**
 * Created by Student on 13.12.2018.
 */
public class NewCoffeeMachine extends CoffeeMachine {

    private final int MILK_CAPACITY;

    private int milkVolume;

    public NewCoffeeMachine(int waterCapacity, int coffeeCapacity, int wasteCapacity, int milkCapacity) {
        super(waterCapacity, coffeeCapacity, wasteCapacity);
        this.MILK_CAPACITY = milkCapacity;
        this.milkVolume = 0;
    }

    public void fillMilk() {
        this.milkVolume = this.MILK_CAPACITY;
        System.out.println("Milk tank is full");
    }

    @Override
    public Coffee makeCoffee(Type type, Scanner scanner) throws NoWaterException, NoCoffeeException, WasteBinOverfillException, NoMilkException {

        Coffee coffee = new Coffee(type);
        if (type == Type.CAPUCCINO || type == Type.LATTE) {
            System.out.println("Enter the amount of milk");
            coffee.setMilk(scanner.nextInt());
        }
        System.out.println("Start making " + coffee.getType());
        if (this.getWaterVolume() < coffee.getWater()) {
            throw new NoWaterException();
        }
        if (this.getCoffeeVolume() < coffee.getCoffee()) {
            throw new NoCoffeeException();
        }
        if (this.getWasteVolume() < coffee.getWaste()) {
            throw new WasteBinOverfillException();
        }
        if (this.milkVolume < coffee.getMilk()) {
            throw new NoMilkException();
        }

        this.setCoffeeVolume(this.getCoffeeVolume() - coffee.getCoffee());
        this.setWaterVolume(this.getWaterVolume() - coffee.getWater());
        this.setWasteVolume(this.getWasteVolume() - coffee.getWaste());
        this.setMilkVolume(this.getMilkVolume() - coffee.getMilk());
        System.out.printf("The remaining volume: %d of water, %d of coffee, %d of milk, %d of waste%n",
                this.getWaterVolume(), this.getCoffeeVolume(), this.milkVolume, this.getWasteVolume());
        return coffee;
    }

    public int getMilkVolume() {
        return milkVolume;
    }

    public void setMilkVolume(int milkVolume) {
        this.milkVolume = milkVolume;
    }
}
