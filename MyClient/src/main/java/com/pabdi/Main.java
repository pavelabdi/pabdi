package com.pabdi;

import com.pabdi.service.Service;
import com.pabdi.service.impl.ServiceImpl1;
import com.pabdi.service.impl.ServiceImpl2;

public class Main {

    public static void main(String[] args) {
        String operation = args[0];
        Long articleId = Long.valueOf(args[1]);
        Long method = Long.valueOf(args[2]);
        String url = "https://jsonplaceholder.typicode.com/posts/";
        if (method == 1) {
            Service service = new ServiceImpl1();
            if (operation.equals("GET")) {
                service.get(url + articleId);
            } else if (operation.equals("POST")) {
                service.post(url);
            } else {
                System.out.println("Wrong operation");
            }
        } else if (method == 2) {
            Service service = new ServiceImpl2();
            if (operation.equals("GET")) {
                service.get(url + articleId);
            } else if (operation.equals("POST")) {
                service.post(url);
            } else {
                System.out.println("Wrong operation");
            }
        } else {
            System.out.println("Wrong method");
        }
    }
}
