package com.pabdi;

import lombok.EqualsAndHashCode;

import java.util.Random;

@EqualsAndHashCode
public class Philosopher implements Runnable {

    private int number;

    private Fork leftFork;

    private Fork rightFork;

    private Waiter[] waiters;

    private Random random;

    private boolean leftForkIsAcquired;

    private boolean rightForkIsAcquired;

    public Philosopher(int number, Fork leftFork, Fork rightFork, Waiter[] waiters) {
        this.number = number + 1;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
        this.waiters = waiters;

        random = new Random(System.nanoTime());
        leftForkIsAcquired = false;
        rightForkIsAcquired = false;
    }

    @Override
    public void run() {
        System.out.println("Philosopher " + number + " sat at the table.");
        while (true) {
            try {
                if (leftFork.availablePermits() > 0) {
                    waiters[0].acquire();
                    leftFork.acquire();
                    leftForkIsAcquired = true;
                    if (rightForkIsAcquired) {
                        System.out.println("Philosopher " + number + " takes the left fork. He now has two forks and eats spaghetti.");
                        Thread.sleep((random.nextInt(10) + 1) * 1000);
                    } else {
                        System.out.println("Philosopher " + number + " takes the left fork. He has one fork.");
                        Thread.sleep((random.nextInt(5) + 1) * 1000);
                    }
                } else {
                    if (!leftForkIsAcquired) {
                        System.out.println("Philosopher " + number + " tries to take the left fork. The left fork is occupied.");
                        Thread.sleep((random.nextInt(5) + 1) * 1000);
                    }
                }
                if (rightFork.availablePermits() > 0) {
                    waiters[1].acquire();
                    rightFork.acquire();
                    rightForkIsAcquired = true;
                    if (leftForkIsAcquired) {
                        System.out.println("Philosopher " + number + " takes the right fork. He now has two forks and eats spaghetti.");
                        Thread.sleep((random.nextInt(10) + 1) * 1000);
                    } else {
                        System.out.println("Philosopher " + number + " takes the right fork. He has one fork.");
                        Thread.sleep((random.nextInt(5) + 1) * 1000);
                    }
                } else {
                    if (!rightForkIsAcquired) {
                        System.out.println("Philosopher " + number + " tries to take the right fork. The right fork is occupied.");
                        Thread.sleep((random.nextInt(5) + 1) * 1000);
                    }
                }
                if (leftForkIsAcquired && rightForkIsAcquired) {
                    leftFork.release();
                    rightFork.release();
                    waiters[0].release();
                    waiters[1].release();
                    leftForkIsAcquired = false;
                    rightForkIsAcquired = false;
                    System.out.println("Philosopher " + number + " has finished eating and is thinking.");
                    Thread.sleep((random.nextInt(5) + 1) * 1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
