package com.pabdi;

public class NoMilkException extends Exception {

    public NoMilkException(){
        super("Not enough milk");
    }
}
