package com.pabdi;

public class NoCoffeeException extends Exception {

    public NoCoffeeException(){
        super("Not enough coffee");
    }
}
