package com.pabdi;

import java.util.Objects;
import java.util.Scanner;

/**
 * Created by Student on 13.12.2018.
 */
public class CoffeeMachine {

    private final int WATER_CAPACITY;

    private int waterVolume;

    private final int COFFEE_CAPACITY;

    private int coffeeVolume;

    private final int WASTE_CAPACITY;

    private int wasteVolume;

    private boolean isOn;

    public CoffeeMachine(int waterCapacity, int coffeeCapacity, int wasteCapacity) {
        this.WATER_CAPACITY = waterCapacity;
        this.waterVolume = 0;
        this.COFFEE_CAPACITY = coffeeCapacity;
        this.coffeeVolume = 0;
        this.WASTE_CAPACITY = wasteCapacity;
        this.wasteVolume = wasteCapacity;
    }

    private boolean checkReady(Coffee coffee) {
        return !(checkWater(coffee) || checkCoffee(coffee) || checkWaste(coffee));
    }

    private boolean checkWaste(Coffee coffee) {
        if (this.wasteVolume < coffee.getWaste()) {
            System.out.println("Clean the waste bin");
            return true;
        }
        return false;
    }

    private boolean checkCoffee(Coffee coffee) {
        if (this.coffeeVolume < coffee.getCoffee()) {
            System.out.println("Not enough coffee");
            return true;
        }
        return false;
    }

    private boolean checkWater(Coffee coffee) {
        if (this.waterVolume < coffee.getWater()) {
            System.out.println("Not enough water");
            return true;
        }
        return false;
    }

    public void switchOn() {
        this.isOn = true;
        System.out.println("The machine is switched on");
    }

    public void switchOff() {
        this.isOn = false;
        System.out.println("The machine is switched off");
    }

    public Coffee makeCoffee(Type type, Scanner scanner) throws NoWaterException, NoCoffeeException, WasteBinOverfillException, NoMilkException {
        Coffee coffee = new Coffee(type);
        if (type == Type.CAPUCCINO || type == Type.LATTE) {
            System.out.println("Can't make it");
            return null;
        }
        System.out.println("Start making " + coffee.getType());
        if (this.waterVolume < coffee.getWater()) {
            throw new NoWaterException();
        }
        if (this.coffeeVolume < coffee.getCoffee()) {
            throw new NoCoffeeException();
        }
        if (this.wasteVolume < coffee.getWaste()) {
            throw new WasteBinOverfillException();
        }

        this.coffeeVolume = this.coffeeVolume - coffee.getCoffee();
        this.waterVolume = this.waterVolume - coffee.getWater();
        this.wasteVolume = this.wasteVolume - coffee.getWaste();
        System.out.printf("The remaining volume: %d of water, %d of coffee, %d of waste%n", this.waterVolume, this.coffeeVolume, this.wasteVolume);
        return coffee;
    }

    public void fillWater() {
        this.waterVolume = this.WATER_CAPACITY;
        System.out.println("Water tank is full");
    }

    public void fillCoffee() {
        this.coffeeVolume = this.COFFEE_CAPACITY;
        System.out.println("Coffee tank is full");
    }

    public void cleanWaste() {
        this.wasteVolume = this.WASTE_CAPACITY;
        System.out.println("Waste bin is empty");
    }

    public int getWaterVolume() {
        return waterVolume;
    }

    public void setWaterVolume(int waterVolume) {
        this.waterVolume = waterVolume;
    }

    public int getCoffeeVolume() {
        return coffeeVolume;
    }

    public void setCoffeeVolume(int coffeeVolume) {
        this.coffeeVolume = coffeeVolume;
    }

    public int getWasteVolume() {
        return wasteVolume;
    }

    public void setWasteVolume(int wasteVolume) {
        this.wasteVolume = wasteVolume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        return (o != null && getClass() == o.getClass());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getClass());
    }

    @Override
    public String toString() {
        return "Coffee machine " + this.getClass();
    }
}
