package com.pabdi;

import lombok.EqualsAndHashCode;

import java.util.concurrent.Semaphore;

@EqualsAndHashCode
public class Fork extends Semaphore {

    public Fork(int permits) {
        super(permits);
    }
}
