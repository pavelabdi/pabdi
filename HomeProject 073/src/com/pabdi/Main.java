package com.pabdi;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //CoffeeMachine machine = new DeLonghi();
        CoffeeMachine machine = new NewDeLonghi();
        //CoffeeMachine machine = new Siemens();
        //CoffeeMachine machine = new NewSiemens();
        startPresentation(machine);
        System.out.println(machine);
    }

    public static void startPresentation(CoffeeMachine machine) {
        machine.switchOn();
        machine.fillWater();
        machine.fillCoffee();
        if (machine instanceof NewCoffeeMachine) {
            ((NewCoffeeMachine) machine).fillMilk();
        }
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Choose coffee type");
            System.out.println("1: Americano");
            System.out.println("2: Espresso");
            System.out.println("3: Capuccino");
            System.out.println("4: Latte");
            System.out.println("5: Exit");
            int choice = input.nextInt();
            if (choice == 5) {
                machine.switchOff();
                break;
            }
            Type type = Type.values()[choice - 1];
            try {
                System.out.println(machine.makeCoffee(type, input));
                System.out.println("Here's your coffee");
                System.out.println();
            } catch (NoWaterException e) {
                e.printStackTrace();
                machine.fillWater();
            } catch (NoCoffeeException e) {
                e.printStackTrace();
                machine.fillCoffee();
            } catch (WasteBinOverfillException e) {
                e.printStackTrace();
                machine.cleanWaste();
            } catch (NoMilkException e) {
                e.printStackTrace();
                ((NewCoffeeMachine) machine).fillMilk();
            }
        }
    }
}
