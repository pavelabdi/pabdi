package com.pabdi.service;

public interface Service {

    void get(String url);

    void post(String url);
}
