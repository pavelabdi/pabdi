package com.pabdi;

public class WasteBinOverfillException extends Exception {

    public WasteBinOverfillException(){
        super("Clean the waste bin");
    }
}
