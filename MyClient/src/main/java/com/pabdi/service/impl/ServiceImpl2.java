package com.pabdi.service.impl;

import com.pabdi.service.Service;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class ServiceImpl2 implements Service {

    @Override
    public void get(String url) {
        HttpClient client = HttpClientBuilder.create().build();
        try {
            //send the request
            HttpGet httpGet = new HttpGet(url);
            HttpResponse response = client.execute(httpGet);
            String result = new BasicResponseHandler().handleResponse(response);

            //parse the response
            JSONParser parser = new JSONParser();
            JSONObject post = (JSONObject) parser.parse(result);
            Long id = (Long) post.get("id");
            Long userId = (Long) post.get("userId");
            String title = (String) post.get("title");
            String body = (String) post.get("body");
            System.out.println("Article " + id + ": User " + userId + " Title \"" + title + "\" Message \"" + body + "\"");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void post(String url) {
        HttpClient client = HttpClientBuilder.create().build();
        try {
            //send the request
            HttpPost httpPost = new HttpPost(url);
            String json = "{\"title\": \"some title\", \"body\": \"some message\", \"userId\": 1}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse response = client.execute(httpPost);

            //parse the response
            String result = new BasicResponseHandler().handleResponse(response);
            JSONParser parser = new JSONParser();
            JSONObject post = (JSONObject) parser.parse(result);
            Long id = (Long) post.get("id");
            Long userId = (Long) post.get("userId");
            String title = (String) post.get("title");
            String body = (String) post.get("body");
            System.out.println("Article " + id + " has been created: User " + userId + " Title \"" + title + "\" Message \"" + body + "\"");

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }
}
