package com.pabdi;

import java.util.Objects;

/**
 * Created by Student on 13.12.2018.
 */
public class Coffee {

    private Type type;

    private int water;

    private int coffee;

    private int milk;

    private int waste;

    public Coffee(Type type) {
        this.type = type;
        if (type == Type.AMERICANO) {
            this.water = 100;
            this.coffee = 22;
            this.waste = 22;
        } else {
            this.water = 30;
            this.coffee = 22;
            this.waste = 22;
        }
    }

    public int getWater() {
        return this.water;
    }

    public int getCoffee() {
        return this.coffee;
    }

    public int getWaste() {
        return this.waste;
    }

    public Type getType() {
        return type;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coffee coffee = (Coffee) o;
        return (this.type == coffee.getType()) && (this.milk == coffee.getMilk());
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, milk);
    }

    @Override
    public String toString() {
        return "Coffee " + type;
    }
}
